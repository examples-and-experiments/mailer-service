#!/bin/sh
set -e

#if [ ${COPY_ENV} = 1 ]; then
#  echo "---- Copied BE .env file ----".
#  cp .env.local.docker .env.local
#else
#  echo "---- Keep your BE .env file ----".
#fi

composer install --ignore-platform-reqs --no-scripts

bin/console ca:cl
symfony server:stop

if [ ${FRESH_DB} = 1 ]; then
  echo "---- Create fresh DB ----".
    symfony console doctrine:database:drop --if-exists --force && symfony console doctrine:database:create && symfony console doctrine:migrations:migrate --no-interaction && symfony console doctrine:fixtures:load --append --quiet
  echo "---- Fresh DB created ----".
else
  echo "---- DB is just as you want it ----".
fi

# first arg is `-f` or `--some-option`
if [ "${1#-}" != "$1" ]; then
	set -- php "$@"
fi

exec "$@"
