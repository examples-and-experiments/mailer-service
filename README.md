# Mailer Service

Mailer service project for learning and experimenting.


## Requirements

    # Requirements depend on how the project is run
    # If the app is run within Docker, than only Docker and docker-compose are required

    PHP >=8.1.0
    Composer
    Symfony CLI
    Docker and docker-compose 


## Run project

### Completely within Docker

**This is the recommended way of running the project.**

Requires Docker and `docker-compose` to be installed on the host system.

Create `.env.dev.local` file as a copy of `.env.docker.example`

For the first time, run the command below that runs Doctrine migration and loads Doctrine data fixtures:

    docker-compose -f docker-compose-clean-start.yml up

    # or run on background
    docker-compose -f docker-compose-clean-start.yml up -d

If you do not want to reset your database when you start containers, run the following command: 

    docker-compose up

    # or run on background
    docker-compose up -d

The system's API should be available at http://127.0.0.1:8200

### Only database within Docker

Requires Composer, Symfony CLI, Docker and `docker-compose` to be installed on the host system.

Create `.env.dev.local` file as a copy of `.env.dev.example`

Run the commands below:

    docker-compose up mailer-service-db

    # or run on background
    docker-compose up mailer-service-db -d

    composer install

    symfony serve

    # Initialize database schema
    bin/console doctrine:migrations:migrate --no-interaction -e dev

    # Load fixtures data into database
    bin/console doctrine:fixture:load --no-interaction -v -e dev

The system should be available at the URL displayed in `symfony serve` output.

![Symfony web server output](./docs/images/symfony_web_server.png "Symfony web server output")

### Without Docker

Requires Composer and Symfony CLI to be installed on the host system.

Moreover, PostgreSQL database with parameters specified in `.env.example` has to run on the host system.  

    composer install
    
    symfony serve

    # Initialize database schema
    bin/console doctrine:migrations:migrate --no-interaction -e dev

    # Load fixtures data into database
    bin/console doctrine:fixture:load --no-interaction -v -e dev

The system should be available at the URL displayed in `symfony serve` output.

![Symfony web server output](./docs/images/symfony_web_server.png "Symfony web server output")
